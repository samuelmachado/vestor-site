import Vue from 'vue';
import App from './App.vue';
import router from './router';
// import 'bootstrap/dist/css/bootstrap.min.css'
import store from './store'
import VoerroTagsInput from '@voerro/vue-tagsinput';
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import vuetify from './plugins/vuetify'
import vSelect from "vue-select";

Vue.config.productionTip = false;
import Swal from 'sweetalert2/dist/sweetalert2.js'

import 'sweetalert2/src/sweetalert2.scss'
Vue.component('tags-input', VoerroTagsInput);
Vue.use(vuetify)

Vue.component("v-select", vSelect);

import VModal from 'vue-js-modal'

Vue.use(VModal)

new Vue({
  Swal,
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
