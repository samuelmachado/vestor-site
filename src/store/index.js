import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import login from './login'
import app from './app'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  storage: window.localStorage,
  modules: ['login', 'app']
})

export default new Vuex.Store({
  modules: {
    login,
    app
  },
  plugins: [vuexLocalStorage.plugin]
})
