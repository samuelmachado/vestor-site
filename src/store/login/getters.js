// Getters do modulo

const numero = (state) => {
  if (state.area && state.celular) {
    return `${state.area}${state.celular.split(' ').join('')}`
  }
  return ''
}

export default { numero }
