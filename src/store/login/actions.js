import { gerarSessao } from '@/services/vestor'

export default {
  enviarCodigo (context) {
    return gerarSessao({
      telefone: context.getters.numero
    })
  }
}
