export default {
    data: () => ({
      errors: []
    }),
    methods: {
      setError ({ msg, key }) {
        this.errors.push({ msg, key })
      },
      removeError (key) {
        for (var i = 0; i < this.errors.length; i++) {
          if (this.errors[i].key == key) {
            this.errors.splice(i, 1)
          }
        }
      },
      clearErrors () {
        this.errors = []
      }
    }
  }