import axios from 'axios'
// import store from '@/store'
//     'Authorization': store.state.app.token ? store.state.app.token : ''
// before a request is made start the nprogress

// axios.interceptors.response.use((response) => {
//   return response
// }, 
// function (error) {
//   // const originalRequest = error.config;
//   //&& !originalRequest._retry
//   if (error.response.status === 401) {
//     localStorage.removeItem('token');
//       // originalRequest._retry = true;
//       // return axios.post('/auth/token',
//       //     {
//       //       "refresh_token": localStorageService.getRefreshToken()
//       //     })
//       //     .then(res => {
//       //         if (res.status === 201) {
//       //             // 1) put token to LocalStorage
//       //             localStorageService.setToken(res.data);

//       //             // 2) Change Authorization header
//       //             axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorageService.getAccessToken();

//       //             // 3) return originalRequest object with Axios.
//       //             return axios(originalRequest);
//       //         }
//       //     })
//   }
// });


let b64DecodeUnicode = str =>
  decodeURIComponent(
    Array.prototype.map.call(atob(str), c =>
      '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    ).join(''))

let parseJwt = async token =>
  await JSON.parse(
    b64DecodeUnicode(
      token.split('.')[1].replace('-', '+').replace('_', '/')
    )
  )


let instance = () => axios.create({
  baseURL: 'http://localhost/vestor-core/backend/web/index.php/api/',
  timeout: 5000,
  headers: {
  }
})

let instanceAuthenticated = () => axios.create({
  baseURL: 'http://localhost/vestor-core/backend/web/index.php/api/',
  timeout: 5000,
  headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
})


const gerarSessao = ({ telefone }) => {
  return instance().post('pins/gerar', {
    telefone: telefone.toString(),
    isTeste: true
  })
}

const enviarPin = ({ pin, telefone,usuario }) => {
  return instance().post('pins/autenticar', {
    pin,
    telefone,
    usuario
  })
}

const enviarProposta = ({ data }) => {
  return instanceAuthenticated().post('contratos/novo', data)
}

const imoveis = ({ filter = null, page=1 }) => {
  // console.warn(filter);

  if(filter){
    filter = toFormUrlEncoded(filter)
    // console.warn(filter);
  } 
  return instance().get(`imoveis/index?fields=id,area,vagaGaragem,quarto,endereco.completo,endereco.bairro.nome,endereco.cidade.nome,valor,thumbnails.arquivo,banheiro,valorCondominio,valorIPTU,valorSeguroIncendio,area,vagaGaragem&page=${page}&${filter}`, {
    page
  })
}

const viewImovel = ({ id }) => {
  return instance().get(`imoveis/view?id=${id}`)
}

const cancelarVisita = ({ id }) => {
  return instanceAuthenticated().get(`visitas/cancelar?id=${id}`)
}
const viewUsuario = () => {
  return instanceAuthenticated().get(`usuario/view`)
}

const getVisitas = () => {
  return instanceAuthenticated().get(`visitas/minhas-visitas`)
}

const novaVisita = ({ data }) => {
  return instanceAuthenticated().post(`visitas/create`, {data})
}


const getMeusImoveis = () => {
  return instanceAuthenticated().get(`imoveis/meus-imoveis?fields=id,area,vagaGaragem,quarto,endereco.completo,endereco.bairro.nome,endereco.cidade.nome,valor,thumbnails.arquivo,banheiro,valorCondominio,valorIPTU,valorSeguroIncendio,area,vagaGaragem,contrato.id,contrato.valorAluguel,contrato.inicio,contrato.fim,idProprietarioUsuario`)
}

const reagendarVisita = ({ data }) => {
  return instanceAuthenticated().post(`visitas/regendar`, {data})
}

/**
 * @param {Object} object
 * @return {string}
 */
 const toFormUrlEncoded = (object) => {
  return Object.entries(object)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
};
export {parseJwt, getMeusImoveis, enviarProposta, instance, gerarSessao, enviarPin, imoveis, viewImovel, novaVisita, viewUsuario, getVisitas, cancelarVisita,reagendarVisita }
 