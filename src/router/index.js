import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Imoveis from '../views/Imoveis.vue';
import Imovel from '../views/Imovel.vue';
import Perfil from '../views/Perfil.vue';
import Favoritos from '../views/Favoritos.vue';
import MinhasVisitas from '../views/MinhasVisitas.vue';
import MeusImoveis from '../views/MeusImoveis.vue';
import Alugar from '../views/Alugar.vue';
import ProcessoLocacao from '../views/ProcessoLocacao'



Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/perfil',
    name: 'Perfil',
    component: Perfil,
  },

  {
    path: '/alugar',
    name: 'Alugar',
    component: Alugar,
  },
  {
    path: '/favoritos',
    name: 'Favoritos',
    component: Favoritos,
  },
  {
    path: '/minhas-visitas',
    name: 'MinhasVisitas',
    component: MinhasVisitas,
  },
  {
    path: '/meus-imoveis',
    name: 'MeusImoveis',
    component: MeusImoveis,
  },
  {
    path: '/imoveis',
    name: 'Imoveis',
    component: Imoveis,
  },
  {
    path: '/processo-locacao',
    name: 'ProcessoLocacao',
    component: ProcessoLocacao,
    props: {imovel: String }
  },
  {
    path: '/imovel/:id',
    name: 'Imovel',
    component: Imovel,
    props: {imovel: String}
  },
  {
    path: '/login',
    component: () => import('@/views/Login.vue'),
    children: [
      {
        path: '',
        name: 'login',
        redirect: 'enviar-sms'
      },
      {
        name: 'login-confirmar-codigo',
        path: 'confimar-codigo',
        component: () => import('@/components/ConfirmarCodigo.vue'),
      },
      {
        name: 'login-enviar-sms',
        path: 'enviar-sms',
        component: () => import('@/components/EnviarSms.vue'),
      },
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});


export default router;
